# UzhEngine

Hobby 2D/3D game engine written in C++.

## Features

1. OpenGL 3D rendering (WIP)
2. Model loading (WIP)
3. 3D Sounds (TODO)
4. Weapons (TODO)
5. Shadows (TODO)
6. Lightning (TODO)
7. Basic FPS gameplay (TODO)

## Code Structure

* /Game - program entry point, gameplay definition
* /ThirdParty - third-party libraries
* /UzhEngine - main engine source tree
  * /Core - core classes (file system, smart-pointer, logger, config loading)
  * /Engine - engine classes
  * /Init - engine DLL export function
  * /Render - all render-related stuff (Shader, Model, Camera, OBJLoader classes)
  * /Scene - scene managment classes

## Controls
* WASD - move camera
* Space - fly higher
* Shift - fly lower
* F2 - enables debug mode (shows camera position, fps, frame time, amount of OpenGL draw calls)
* F10 - release mouse

## Code flow

/Game module links against UzhEngine.dll and calls the only one exported function: **InitializeUzhEngine()**, which returns IEngineInterface implemenation, required to init, run and destroy engine. See code in [Game/Main.cpp](https://github.com/MrOnlineCoder/UzhEngine/blob/master/Game/Main.cpp)

[EngineInterface](https://github.com/MrOnlineCoder/UzhEngine/blob/master/UzhEngine/Engine/EngineInterface.cpp) loads config, inits engine and contains game engine loop:

```cpp
while (engine->isRunning()) {
		engine->tick();
		engine->render();
}
```

[Engine](https://github.com/MrOnlineCoder/UzhEngine/blob/master/UzhEngine/Engine/Engine.cpp) does all necessary init routines: creates render window, loads OpenGL functions, game data and implements tick function.



## Third-party libraries

* SFML - window creation, input handling, 2D graphics ([sfml-dev.org](http://sfml-dev.org))

* GLM (OpenGL Mathematics) - vectors, matrices, transformations ([glm.g-truc.net](https://glm.g-truc.net/))

* gl3w - OpenGL functions loading ([gl3w github](https://github.com/skaslev/gl3w))

* tiny-obj-loader - .OBJ model loading ([tinyobjloader github](https://github.com/syoyo/tinyobjloader))
