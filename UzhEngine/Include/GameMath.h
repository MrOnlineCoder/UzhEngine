/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Math.h
	Description: Helper math definitions
	==============================================================================
*/

#pragma once

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <glm/mat2x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <glm/gtx/string_cast.hpp>
#include <glm/ext.hpp>

namespace uzh {
	typedef glm::vec2 vec2;
	typedef glm::vec3 vec3;
	typedef glm::vec4 vec4;

	typedef glm::mat2 mat2;
	typedef glm::mat3 mat3;
	typedef glm::mat4 mat4;

	

	struct Transform {
		uzh::vec3 pos;
		uzh::vec3 rotation;
		uzh::vec3 scale;
	};

	class Math {
		public:
			static uzh::vec3 zero3() {
				return uzh::vec3(0.0f, 0.0f, 0.0f);
			}
		
			static uzh::mat4 makeModelMatrix(uzh::Transform& t) {
				uzh::mat4 mat(1.0f);

				mat = glm::scale(mat, t.scale);

				mat = glm::rotate(mat, glm::radians(t.rotation.x), uzh::vec3(1, 0, 0));
				mat = glm::rotate(mat, glm::radians(t.rotation.y), uzh::vec3(0, 1, 0));
				mat = glm::rotate(mat, glm::radians(t.rotation.z), uzh::vec3(0, 0, 1));

				mat = glm::translate(mat, t.pos);

				return mat;
			}
	};

	

	
}