/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: GL.h
	Description: OpenGL related stuff
	==============================================================================
*/

#pragma once

#include <GL/gl3w.h>

static int GLverticesCount = 0;

namespace uzh {
	class GL {
	public:
		static int loadFuncs() {
			return gl3wInit();
		}

		static void renderArrays(GLuint size) {
			glDrawArrays(GL_TRIANGLES, 0, size);
			GLverticesCount += size;
		}

		static void renderElements(GLuint size) {
			glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, 0);
		}

		static void zeroTexture() {
			glActiveTexture(GL_TEXTURE0);
		}

		static void clearTarget() {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

			GLverticesCount = 0;
		}

		static void bindVAO(GLuint vao) {
			glBindVertexArray(vao);
		}

		static void bindVBO(GLuint vbo) {
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
		}

		static void resetObjects() {
			bindVBO(0);
			bindVAO(0);
			glUseProgram(0);
			zeroTexture();
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		static void enableDepth() {
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
		}

		static void enableCulling() {
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
		}
	};
}