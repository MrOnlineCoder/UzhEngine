/*
UzhEngine Project

MIT License

Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

==============================================================================
File: OBJLoader.h
Description: Loads .obj file as model
==============================================================================
*/

#pragma once

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>
#include "OBJLoader.h"

bool uzh::OBJLoader::loadFromFile(std::string file) {
	std::string err;
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	LOG.tag("OBJLoader") << "Loading .obj file "+file;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, file.c_str());
	if (!err.empty()) {
		LOG.tag("OBJLoader") << "ERROR: failed to load " << file << ": " << err;
		return false;
	}


	for (const auto& shape : shapes) {
		for (const auto& index : shape.mesh.indices) {
			mesh.vertices.push_back(attrib.vertices[3 * index.vertex_index + 0]);
			mesh.vertices.push_back(attrib.vertices[3 * index.vertex_index + 1]);
			mesh.vertices.push_back(attrib.vertices[3 * index.vertex_index + 2]);

			if (attrib.texcoords.size() > 0) {
				mesh.texCoords.push_back(attrib.texcoords[2 * index.texcoord_index + 0]);
				mesh.texCoords.push_back(1.0f - attrib.texcoords[2 * index.texcoord_index + 1]);
			}

			if (attrib.normals.size() > 0) {
				mesh.normals.push_back(attrib.normals[3 * index.normal_index + 0]);
				mesh.normals.push_back(attrib.normals[3 * index.normal_index + 1]);
				mesh.normals.push_back(attrib.normals[3 * index.normal_index + 2]);
			}
		}
	}

	LOG.tag("OBJLoader") << "Loaded file with " << attrib.vertices.size() << " vertices, " << attrib.normals.size() << " normals, " << attrib.texcoords.size() << " tex coords";
	LOG.tag("OBJLoader") << "Mesh contains " << mesh.getVerticesCount3d() << " vertices, " << mesh.normals.size() << " normals, " << attrib.texcoords.size() << " tex coords";
	
	return true;
}

uzh::Mesh & uzh::OBJLoader::getMesh() {
	return mesh;
}
