/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Skybox.cpp
	Description: Represents 3d skybox around the world
	==============================================================================
*/

#pragma once

#include <string>
#include <GL/glcorearb.h>
#include "Model.h"
#include "Skybox.h"

const float SKYBOX_SIZE = 50.0f;

uzh::Skybox::Skybox() {

}

void uzh::Skybox::create() {
	LOG.tag("Skybox") << "Creating Skybox with size " << SKYBOX_SIZE;
	model.genBuffers();

	static float skyboxVertices[] = {
		//Front
		SKYBOX_SIZE, SKYBOX_SIZE, SKYBOX_SIZE,
		SKYBOX_SIZE, -SKYBOX_SIZE, SKYBOX_SIZE,
		-SKYBOX_SIZE, SKYBOX_SIZE, SKYBOX_SIZE,
		-SKYBOX_SIZE, -SKYBOX_SIZE, SKYBOX_SIZE,
		//Back
		-SKYBOX_SIZE, SKYBOX_SIZE, -SKYBOX_SIZE, 
		-SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE, 
		SKYBOX_SIZE, SKYBOX_SIZE, -SKYBOX_SIZE, 
		SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
		//Left
		-SKYBOX_SIZE, SKYBOX_SIZE, SKYBOX_SIZE, 
		-SKYBOX_SIZE, -SKYBOX_SIZE, SKYBOX_SIZE, 
		-SKYBOX_SIZE, SKYBOX_SIZE, -SKYBOX_SIZE, 
		-SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
		//Right
		SKYBOX_SIZE, SKYBOX_SIZE, -SKYBOX_SIZE, 
		SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE, 
		SKYBOX_SIZE, SKYBOX_SIZE, SKYBOX_SIZE, 
		SKYBOX_SIZE, -SKYBOX_SIZE, SKYBOX_SIZE,
		//Top
		-SKYBOX_SIZE, SKYBOX_SIZE, -SKYBOX_SIZE, 
		SKYBOX_SIZE, SKYBOX_SIZE, -SKYBOX_SIZE, 
		-SKYBOX_SIZE, SKYBOX_SIZE, SKYBOX_SIZE, 
		SKYBOX_SIZE, SKYBOX_SIZE, SKYBOX_SIZE,
		//Bottom
		SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE, 
		-SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
		SKYBOX_SIZE, -SKYBOX_SIZE, SKYBOX_SIZE,
		-SKYBOX_SIZE, -SKYBOX_SIZE, SKYBOX_SIZE
	};

	static float skyboxTexcoords[] = {
		//Front
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		//Back
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		//Left
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		//Right
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		//Top
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		//Bottom
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f
	};
	
	Mesh m;
	m.vertices = std::vector<GLfloat>(skyboxVertices, skyboxVertices + (6 * 4 * 3));
	m.texCoords = std::vector<GLfloat>(skyboxTexcoords, skyboxTexcoords + (6 * 4 * 2));

	model.addMesh(m);

	LOG.tag("Skybox") << "Loading skybox shader...";
	shader.loadVertexSource("Content/Shaders/skybox.vert");
	shader.loadFragmentSource("Content/Shaders/skybox.frag");
	shader.link();
}

void uzh::Skybox::loadSide(std::string file, SkyboxSides side) {
	uzh::LOG.tag("Skybox") << "Loading skybox side #" << side << " from " << file;
	if (!sideTextures[side].loadFromFile(file)) {
		uzh::LOG.tag("Skybox") << "ERROR: Failed to load skybox texture!";
	}

	sideTextures[side].setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	sideTextures[side].setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	sideTextures[side].setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	sideTextures[side].setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void uzh::Skybox::loadFromFolder(std::string path, std::string format) {
	loadSide(path+"/front."+format, SkyboxSides::Front);
	loadSide(path + "/left." + format, SkyboxSides::Left);
	loadSide(path + "/right." + format, SkyboxSides::Right);
	loadSide(path + "/top." + format, SkyboxSides::Top);
	loadSide(path + "/bottom." + format, SkyboxSides::Bottom);
	loadSide(path + "/back." + format, SkyboxSides::Back);
}

void uzh::Skybox::render(uzh::mat4& pmat, uzh::mat4& vmat) {
	shader.bind();

	//Remove translation part of matrix
	shader.loadViewMat(glm::mat4(glm::mat3(vmat)));
	shader.loadProjectionMat(pmat);

	glDepthMask(0);

	model.bind();
	for (int i = 0; i < 6;i++) {
		sideTextures[i].bind();

		glDrawArrays(GL_TRIANGLE_STRIP, i * 4, 4);
	}

	glDepthMask(1);
}
