/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Renderer.h
	Description: Master renderer, which renders all game objects
	==============================================================================
*/

#pragma once

#include <SFML/Graphics.hpp>

#include "Model.h"
#include "Camera.h"
#include "GameMath.h"
#include "GL.h"
#include "Shader.h"
#include "Texture.h"

namespace uzh {
	class Renderer {
	public:
		void create(sf::RenderWindow& wnd, uzh::Camera& cam);
		void setShader(uzh::Shader& arg);
		void setTexture(uzh::Texture& tex);
		void loadTransform(uzh::Transform& t);
		void renderEntityModel(uzh::Model& m, uzh::Transform& transform);
		void renderModel(uzh::Model &m);
		void renderText(sf::Text& txt);
		void renderSprite(sf::Sprite& spr);
		void clear();
	private:
		sf::RenderWindow* window;
		uzh::Camera* camera;
		uzh::Shader* shader;
		uzh::Texture* texture;
		uzh::Transform transform;
	};
}