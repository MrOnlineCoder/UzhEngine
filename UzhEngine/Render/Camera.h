/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Camera.h
	Description: 3D Camera class
	==============================================================================
*/

#pragma once

#include <GL/gl3w.h>
#include <GameMath.h>

#include "../Core/Logger.h"
#include "../Scene/Entity.h"

namespace uzh {
	class Camera {
	public:
		Camera();
		void update(float delta);
		void move(uzh::vec3 vec);

		//Attach NULL to detach
		void attach(uzh::Entity& ent);

		void setPosition(uzh::vec3 vec);

		void rotateX(float angle);
		void rotateY(float angle);

		void moveX(int way);
		void moveZ(int way);
		void moveY(int v);

		void setFOV(float arg);
		void setAspectRatio(int w, int h);
		void setPlanes(float farArg, float nearArg);

		void activate();
		void deactivate();
		bool isActive();

		const float getSpeed() {
			return 5.0f;
		}

		uzh::vec3& getPosition();
		uzh::vec3& getRotation();
		uzh::mat4& getProjViewMatrix();
		uzh::mat4& getViewMatrix();
		uzh::mat4& getProjMatrix();
	private:
		bool active;
		void computeProjection();

		uzh::mat4 viewMat;
		uzh::mat4 projMat;
		uzh::mat4 projViewMat;
		
		uzh::vec3 pos;
		uzh::vec3 rot;
		uzh::vec3 accel;

		float fov;
		float ratio;

		float delta;

		float nearPlane;
		float farPlane;

		uzh::vec3 up;
		uzh::vec3 right;

		uzh::Entity* entity;
	};
}
