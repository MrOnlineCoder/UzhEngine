/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Camera.cpp
	Description: 3D Camera class
	==============================================================================
*/

#pragma once

#include "Camera.h"

uzh::Camera::Camera() {
	pos = uzh::vec3(0.0f, 1.0f, 1.0f);
	fov = 45.0f;

	ratio = 1280 / 720;
	nearPlane = 1.0f;
	farPlane = 100.0f;

	up = uzh::vec3(0,1,0);
	right = uzh::vec3(1,0,0);

	accel = uzh::vec3(0.0f);

	active = true;
	entity = NULL;
}

void uzh::Camera::update(float deltaTime) {
	if (entity != NULL) {
		pos = entity->transform.pos;
		rot = entity->transform.rotation;
	}

	viewMat = uzh::mat4(1.0f);
	viewMat = glm::rotate(viewMat, glm::radians(rot.x), uzh::vec3(1, 0, 0));
	viewMat = glm::rotate(viewMat, glm::radians(rot.y), uzh::vec3(0, 1, 0));
	viewMat = glm::rotate(viewMat, glm::radians(rot.z), uzh::vec3(0, 0, 1));
	viewMat = glm::translate(viewMat, -pos);

	//projViewMat = projMat * viewMat;

	accel = uzh::vec3(0.0f);

	delta = deltaTime;
}

void uzh::Camera::moveY(int v) {
	pos.y += v * getSpeed() * delta;
}

void uzh::Camera::setPosition(uzh::vec3 vec) {
	pos = vec;
}

void uzh::Camera::activate() {
	active = true;
}

void uzh::Camera::deactivate() {
	active = false;
}

bool uzh::Camera::isActive() {
	return active;
}

void uzh::Camera::computeProjection() {
	projMat = glm::perspective(glm::radians(fov), ratio, nearPlane, farPlane);
}

void uzh::Camera::setFOV(float arg) {
	fov = arg;

	//Update projMatrix
	computeProjection();

	LOG.tag("Camera") << "Set camera FOV to " << fov;
}

void uzh::Camera::setAspectRatio(int w, int h) {
	ratio = (float) w / h;

	//Update proj matrix
	computeProjection();

	LOG.tag("Camera") << "Changed aspect ratio to " << w << " / " << h << " = " << ratio;
}

void uzh::Camera::move(uzh::vec3 vec) {
	pos.x = pos.x + vec.x;
	pos.y = pos.y + vec.y;
	pos.z = pos.z + vec.z;
}

void uzh::Camera::attach(uzh::Entity & ent) {
	entity = &ent;
}

uzh::vec3& uzh::Camera::getPosition() {
	return pos;
}

uzh::vec3 & uzh::Camera::getRotation() {
	return rot;
}

void uzh::Camera::rotateX(float angle) {
	rot.x += angle;

	if (rot.x > 90) rot.x = 90;
	if (rot.x < -90) rot.x = -90;
}

void uzh::Camera::moveX(int way) {
	accel.x += way * glm::cos(glm::radians(rot.y)) * getSpeed();
	accel.z += way * glm::sin(glm::radians(rot.y)) * getSpeed();

	pos += (accel * delta);
}

void uzh::Camera::moveZ(int way) {;
	accel.x += -way * glm::cos(glm::radians(rot.y + 90)) * getSpeed();
	accel.z += -way * glm::sin(glm::radians(rot.y + 90)) * getSpeed();

	if (rot.x > 90) rot.x = 90;
	if (rot.x < -90) rot.x = -90;

	pos += (accel * delta);
}

void uzh::Camera::rotateY(float angle) {
	rot.y += angle;
}

uzh::mat4& uzh::Camera::getProjViewMatrix() {
	return getProjMatrix() * getViewMatrix();
}

uzh::mat4& uzh::Camera::getViewMatrix() {
	return viewMat;
}

void uzh::Camera::setPlanes(float farArg, float nearArg) {
	nearPlane = nearArg;
	farPlane = farArg;

	computeProjection();

	LOG.tag("Camera") << "Set camera frustrum planes to: near=" << nearPlane << ", far=" << farPlane;
}

uzh::mat4&uzh::Camera::getProjMatrix() {
	return projMat;
}

