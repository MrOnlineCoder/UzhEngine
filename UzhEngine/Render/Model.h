/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Model.h
	Description: OpenGL 3D model class
	==============================================================================
*/

#pragma once

#include "GL.h"
#include "Mesh.h"

#include "../Core/Logger.h"

namespace uzh {
	class Model {
	public:
		Model();
		~Model();

		void genBuffers();

		void addMesh(uzh::Mesh& mesh);

		void addVertices(float vertices[], std::size_t size);
		void addTexCoords(float* coords, std::size_t size);
		void addIndices(int* data, std::size_t size);


		void bind();

		unsigned int getIndicesCount();
		unsigned int getVerticesCount();
	private:
		void deleteBuffers();

		GLuint vao;
		GLuint vbo;
		GLuint nvbo;
		GLuint ebo;
		GLuint texVBO;

		unsigned int indices;
		unsigned int verticesCount;
	};
}