/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Model.cpp
	Description: OpenGL 3D model class
	==============================================================================
*/

#pragma once

#include "Model.h"

uzh::Model::Model() {
	vbo = -1;
	vao = -1;
	ebo = -1;
	texVBO = -1;
	nvbo = -1;
}

uzh::Model::~Model() {
	deleteBuffers();
}

void uzh::Model::genBuffers() {
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &texVBO);
	glGenBuffers(1, &ebo);
	glGenBuffers(1, &nvbo);

	LOG.tag("Model") << "Generated buffers for model: vbo = " << vbo << ", vao = " << vao << ", ebo=" << ebo << ", texVBO = " << texVBO;
}

void uzh::Model::addMesh(uzh::Mesh & mesh) {
	GL::bindVAO(vao);
	GL::bindVBO(vbo);

	//Bind vertex positions
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * mesh.vertices.size(), mesh.vertices.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);

	glEnableVertexAttribArray(0);


	//Bind texture coords
	if (mesh.texCoords.size() > 0) {
		GL::bindVBO(texVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * mesh.texCoords.size(), mesh.texCoords.data(), GL_STATIC_DRAW);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(1);
	}
	

	//And finally normals
	if (mesh.normals.size() > 0) {
		GL::bindVBO(nvbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * mesh.normals.size(), mesh.normals.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(2);
	}

	LOG.tag("Model") << "Added mesh with " << mesh.getVerticesCount3d() << " vertices, " << mesh.texCoords.size() << " tex coords and " << mesh.normals.size() << " normals.";

	GL::resetObjects();

	//Important
	//
	//We cannot use mesh.vertices.size(), because vertices is really vector of FLOATS, not Vec3. 
	verticesCount = mesh.getVerticesCount3d();
}

void uzh::Model::addVertices(float vertices[], std::size_t size) {
	GL::bindVAO(vao);
	GL::bindVBO(vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * size, &vertices[0], GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);

	glEnableVertexAttribArray(0);

	verticesCount = size / 3;
}

void uzh::Model::addTexCoords(float* coords, std::size_t size) {
	GL::bindVAO(vao);
	GL::bindVBO(texVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * size, coords, GL_STATIC_DRAW);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
}

void uzh::Model::addIndices(int* data, std::size_t size) {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * size, data, GL_STATIC_DRAW);

	indices = size;
}

void uzh::Model::bind() {
	GL::bindVAO(vao);
}

unsigned int uzh::Model::getIndicesCount() {
	return indices;
}

unsigned int uzh::Model::getVerticesCount()
{
	return verticesCount;
}

void uzh::Model::deleteBuffers() {
	if (vbo != -1) glDeleteBuffers(1, &vbo);
	if (texVBO != -1) glDeleteBuffers(1, &texVBO);
	if (vao != -1) glDeleteVertexArrays(1, &vao);
	if (ebo != -1) glDeleteBuffers(1, &ebo);
	if (nvbo != -1) glDeleteBuffers(1, &nvbo);

	vbo = -1;
	vao = -1;
	ebo = -1;
	texVBO = -1;
	nvbo = -1;
}
