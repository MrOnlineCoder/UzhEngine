/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Renderer.cpp
	Description: Master renderer, which renders all game objects
	==============================================================================
*/

#pragma once

#include "Renderer.h"

void uzh::Renderer::create(sf::RenderWindow& wnd, uzh::Camera& cam) {
	window = &wnd;
	camera = &cam;
}

void uzh::Renderer::setShader(uzh::Shader& arg) {
	shader = &arg;
}

void uzh::Renderer::setTexture(uzh::Texture& tex) {
	texture = &tex;
}

void uzh::Renderer::loadTransform(uzh::Transform & t) {
	transform = t;

	shader->loadModelMat(Math::makeModelMatrix(t));
}

void uzh::Renderer::renderEntityModel(uzh::Model& m, uzh::Transform& transform) {
	texture->bind();
	shader->bind();

	loadTransform(transform);

	shader->loadViewMat(camera->getViewMatrix());
	shader->loadProjectionMat(camera->getProjMatrix());

	m.bind();

	GL::renderArrays(m.getVerticesCount());
}

void uzh::Renderer::renderModel(uzh::Model & m) {
	texture->bind();

	m.bind();

	GL::renderArrays(m.getVerticesCount());
}

void uzh::Renderer::renderText(sf::Text& txt) {
	window->draw(txt);
}

void uzh::Renderer::renderSprite(sf::Sprite& spr) {
	window->draw(spr);
}

void uzh::Renderer::clear() {
	GL::clearTarget();
	GL::zeroTexture();
}