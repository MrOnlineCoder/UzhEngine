/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Shader.h
	Description: GLSL shader wrapper
	==============================================================================
*/

#pragma once

#include <string>
#include <GL/gl3w.h>
#include <GameMath.h>
#include <fstream>
#include "../Core/FileSystem.h"
#include "../Core/Logger.h"
#include <sstream>

namespace uzh {

	const std::string MAT_V_LOCATION = "viewMat";
	const std::string MAT_P_LOCATION = "projectionMat";
	const std::string MAT_M_LOCATION = "modelMat";

	const std::string LIGHTNING_DIFFUSE_LOCATION = "lightPointPosition";

	class Shader {
	public:
		Shader();
		bool loadVertexSource(std::string path);
		bool loadFragmentSource(std::string path);
		bool link();
		void bind();

		void loadInteger(const std::string& loc, int n);
		void loadFloat(const std::string& loc, float n);

		void loadVector2(const std::string& loc, uzh::vec2& vec);
		void loadVector3(const std::string& loc, uzh::vec3& vec);
		void loadVector4(const std::string& loc, uzh::vec4& vec);

		void loadMatrix(const std::string& loc, uzh::mat4& mat);

		//Camera specific
		void loadViewMat(uzh::mat4& mat);
		void loadProjectionMat(uzh::mat4& mat);
		void loadModelMat(uzh::mat4& mat);

	private:
		GLuint program;
		GLuint vertex;
		GLuint fragment;

		bool linked;

		std::ofstream file;

		bool checkShader(GLuint id, bool isProgram);
	};
}