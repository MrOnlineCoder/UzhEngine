/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: EngineInterface.cpp
	Description: Engine Interface class
	==============================================================================
*/


#include "EngineInterface.h"

UzhEngineInterface::UzhEngineInterface()
	: engine(new uzh::Engine()){

}

bool UzhEngineInterface::init(int argc, char* argv[]) {
	uzh::LOG.init();

	uzh::LOG.tag("InterfaceInit") << "Initializing UzhEngine...";

	uzh::LOG.tag("InterfaceInit") << "Loading config.cfg..";
	if (!uzh::FileSystem::fileExists("config.cfg")) {
		uzh::LOG.tag("InterfaceInit") << "Config missing, creating new one...";
		uzh::FileSystem::createFile("config.cfg");

		uzh::LOG.tag("InterfaceInit") << "Saving default config...";
		std::ofstream ocfg("config.cfg");
		ocfg << "width = 1280 \nheight = 720\nfullscreen = FALSE\nmaxFps = 60" << std::endl;
		ocfg.close();

	}

	uzh::LOG.tag("InterfaceInit") << "Opening config file..";
	//Real config
	uzh::Configuration cfg;
	uzh::EngineInitOptions opts;

	cfg.loadFromFile("config.cfg");
	cfg.get("width", opts.winWidth);
	cfg.get("height", opts.winHeight);
	cfg.get("fullscreen", opts.fullscreen);
	cfg.get("maxFps", opts.maxFps);

	opts.glMinor = 1;
	opts.glMajor = 3;

	uzh::LOG.tag("InterfaceInit") << "Initliazing engine instance...";

	if (!engine->init(opts)) {
		std::string err = engine->getLastError();
		MessageBox(NULL, err.c_str(), "UzhEngine Init Error", MB_ICONERROR | MB_OK);
		uzh::LOG.close();
		return false;
	}

	return true;
}

int UzhEngineInterface::run() {
	uzh::LOG.tag("InterfaceInit") << "Entering engine loop...";

	while (engine->isRunning()) {
		engine->tick();
		engine->render();
	}
	uzh::LOG.tag("InterfaceInit") << "Engine loop ended. Cleaning up...";

	engine->cleanup();

	uzh::LOG.tag("InterfaceInit") << "Session ended. Exiting with 0x0";
	uzh::LOG.close();

	return 0x0;
}

