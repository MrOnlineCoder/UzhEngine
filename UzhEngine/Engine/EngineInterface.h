#pragma once

#include <UzhEngine.h>

#include "../Core/Configuration.h"
#include "../Core/FileSystem.h"
#include "../Core/Logger.h"

#include "../Core/Pointer.h"
#include "../Engine/Engine.h"

class UzhEngineInterface : public IEngineInterface {
public:
	UzhEngineInterface();
	bool init(int argc, char* argv[]);
	int run();
private:
	uzh::Ptr<uzh::Engine> engine;
};
