/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Engine.cpp
	Description: Contains main class that implements init and game loop
	==============================================================================
*/

#include "Engine.h"

namespace uzh {
	uzh::Engine* UzhEngine;
}


bool uzh::Engine::init(EngineInitOptions opts) {
#ifdef _WIN32
	if (!engineLock.initAndCheck()) {
		error = "The engine is already running. Only one instance of the engine can work simultaneously.";
		return false;
	}
#endif

	engineClock.restart();
	uzh::LOG.tag("Engine") << "init() called, setting up engine environment...";

	error = "! no errors !";
	running = false;

	this->opts = opts;

	uzh::LOG.tag("Engine") << "Passed options: width = " << opts.winWidth << " height = " << opts.winHeight;

	if (!initGraphics()) return false;

	shader.loadVertexSource("Content/Shaders/test.vert");
	shader.loadFragmentSource("Content/Shaders/test.frag");
	shader.link();
	
	tex.loadFromFile("Content/teapot.png");
	debugFnt.loadFromFile("Content/consola.ttf");
	debugTxt.setPosition(10,10);
	debugTxt.setFont(debugFnt);
	debugTxt.setCharacterSize(18);
	debugTxt.setFillColor(sf::Color::Yellow);
	
	skybox.create();
	skybox.loadFromFolder("Content/Skybox/ely_hills" ,"tga");

	model.genBuffers();

	uzh::OBJLoader loader;
	if (loader.loadFromFile("Content/teapot.obj_model")) model.addMesh(loader.getMesh());

	uzh::OBJLoader ak47;
	ak.genBuffers();
	if (ak47.loadFromFile("Content/Ak-47.obj")) ak.addMesh(ak47.getMesh());

	spr.setTexture(tex2);
	spr.setPosition(10,10);

	cam.setAspectRatio(opts.winWidth, opts.winHeight);
	cam.setFOV(45.0f);
	cam.setPlanes(200.0f, 0.1f);
	cam.setPosition(uzh::vec3(0.0f, 0.0f, 10.0f));

	running = true;
	debug = false;

	uzh::LOG.tag("Engine") << "Engine initialization finished";

	UzhEngine = this;

	game.onInit();

	return true;
}

void uzh::Engine::tick() { 
	while (window.pollEvent(ev)) {
		
		//Always handle system window close event
		if (ev.type == sf::Event::Closed) {
			running = false;
			return;
		}

		game.onInput(ev);

		if (ev.type == sf::Event::KeyReleased) {
			if (ev.key.code == sf::Keyboard::F10) {
				if (cam.isActive()) {
					cam.deactivate();
					window.setMouseCursorVisible(true);
				} else {
					cam.activate();
					window.setMouseCursorVisible(false);
				}
				return;
			}

			if (ev.key.code == sf::Keyboard::Space) {
				
			}

			if (ev.key.code == sf::Keyboard::F2) {
				debug = !debug;
				if (debug) {
					glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				} else {
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				}
			}
		}

		sceneManager.processInput(ev);
	}

	if (cam.isActive() && window.hasFocus()) {
		sf::Mouse::setPosition(sf::Vector2i(opts.winWidth/2, opts.winHeight/2),window);
		cam.update(deltaTime);
	}

	/*if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		cam.moveZ(1);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		cam.moveZ(-1);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		cam.moveX(1);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		cam.moveX(-1);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		cam.moveY(1);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
		cam.moveY(-1);
	}*/

	//Update the game

	rot += 0.1f;

	sceneManager.update();

	if (debug) {
		std::stringstream ss;
		ss << "FPS: " << 1000000 / fpsTime << "\n" << fpsTime << " mcS.\ncamera pos: " << glm::to_string(cam.getPosition()) << "\ncamera rot: " << glm::to_string(cam.getRotation()) << "\n";
		ss << "vertices: " << GLverticesCount;
		debugTxt.setString(ss.str());
	}
}

void uzh::Engine::render() {
	GL::clearTarget();
	GL::zeroTexture();

	game.onBeginFrame();
	
	skybox.render(cam.getProjMatrix(), cam.getViewMatrix());

	tex.bind();

	glm::mat4 m(1.0f);
	//m = glm::rotate(m, glm::radians(rot), glm::vec3(1.0f, 0.0f, 0.0f));
	//m = glm::scale(glm::mat4(1.0f), glm::vec3(0.05f, 0.05f, 0.05f));

	shader.bind();
	shader.loadModelMat(m);
	shader.loadProjectionMat(cam.getProjMatrix());
	shader.loadViewMat(cam.getViewMatrix());
	
	model.bind();
	GL::renderArrays(model.getVerticesCount());

	game.onEndFrame();

	if (debug) {
		GL::resetObjects();
		window.pushGLStates();

		
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		window.draw(debugTxt);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		window.popGLStates();
	}

	window.display();
	frameTime = frameClock.restart();
	deltaTime = frameTime.asSeconds();
	fpsTime = frameTime.asMicroseconds();
}

bool uzh::Engine::isRunning() {
	return running;
}

void uzh::Engine::cleanup() {
	game.onDestroy();
	engineLock.release();
}

const std::string & uzh::Engine::getLastError() {
	return error;
}

sf::RenderWindow& uzh::Engine::getWindow() {
	return window;
}

uzh::Renderer& uzh::Engine::getRenderer() {
	return renderer;
}

uzh::Camera& uzh::Engine::getCamera() {
	return cam;
}

uzh::SceneManager& uzh::Engine::getSceneManager() {
	return sceneManager;
}

uzh::EngineInitOptions& uzh::Engine::getOptions() {
	return opts;
}

float uzh::Engine::getDeltaTime() {
	return deltaTime;
}

bool uzh::Engine::initGraphics() {
	uzh::LOG.tag("Engine") << "Creating window context settings...";

	sf::ContextSettings contextSettings;
	contextSettings.antialiasingLevel = 4;
	contextSettings.depthBits = 24;
	contextSettings.stencilBits = 8;

	// Use OpenGL 3.1
	contextSettings.majorVersion = 3;
	contextSettings.minorVersion = 1;


	uzh::LOG.tag("Engine") << "Creating window style...";
	sf::Uint32 style;
	if (opts.fullscreen) {
		style = sf::Style::Fullscreen;
	}
	else {
		style = sf::Style::Close | sf::Style::Titlebar;
	}

	uzh::LOG.tag("Engine") << "Creating RenderWindow...";

	window.create(sf::VideoMode(opts.winWidth, opts.winHeight), "UzhEngine App", style, contextSettings);
	window.setFramerateLimit(opts.maxFps);
	window.setActive(true);
	window.setVerticalSyncEnabled(false);

	uzh::LOG.tag("Engine") << "Loading OpenGL functions...";

	int gl3wErrorCode = uzh::GL::loadFuncs();
	if (gl3wErrorCode) {
		error = "gl3wInit() failed - cannot load OpenGL functions: " + std::to_string(gl3wErrorCode);
		return false;
	}

	GL::enableDepth();
	GL::enableCulling();

	window.setMouseCursorVisible(false);

	uzh::LOG.tag("Engine") << "Creating Renderer..";
	renderer.create(window, cam);

	return true;
}
