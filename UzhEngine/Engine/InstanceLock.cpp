/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: InstanceLock.cpp
	Description: Provides simple interface for limiting amount of engine instances
	==============================================================================
*/

#include "InstanceLock.h"

bool uzh::InstanceLock::initAndCheck() {
	LOG.tag("InstanceLock") << "Checking engine lock mutex...";
	mutex = CreateMutex(NULL, TRUE, "UzhEngineInstanceLockMutex");
	if (mutex == NULL || GetLastError() == ERROR_ALREADY_EXISTS) {
		LOG.tag("InstanceLock") << "Another instance is already running, aborting!";
		return false;
	} else {
		LOG.tag("InstanceLock") << "Mutex created.";
		return true;
	}
}

void uzh::InstanceLock::release() {
	ReleaseMutex(mutex);
	LOG.tag("InstanceLock") << "Mutex released.";
}
