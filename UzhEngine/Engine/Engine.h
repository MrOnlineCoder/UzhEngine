/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Engine.h
	Description: Contains main class that implements init and game loop
	==============================================================================
*/
#pragma once

#include "../Core/Logger.h"
#include "../Render/GL.h"
#include "../Render/Shader.h"
#include "../Render/Texture.h"
#include "../Render/Model.h"
#include "../Render/Camera.h"
#include "../Render/OBJLoader.h"
#include "../Render/Skybox.h"
#include "../Render/Renderer.h"

#include "../Scene/SceneManager.h"

#include "../Game/Game.h"

#include "InstanceLock.h"

#include <SFML/Graphics.hpp>

#include <sstream>

namespace uzh {
	struct EngineInitOptions {
		int winWidth;
		int winHeight;
		bool fullscreen;
		int maxFps;
		
		int glMajor;
		int glMinor;
	};

	class Engine {
	public:
		bool init(EngineInitOptions opts);

		void tick();
		void render();

		bool isRunning();

		void cleanup();

		const std::string& getLastError();

		sf::RenderWindow& getWindow();
		uzh::Renderer& getRenderer();
		uzh::Camera& getCamera();
		uzh::SceneManager& getSceneManager();
		uzh::EngineInitOptions& getOptions();

		float getDeltaTime();
	private:
		bool running;
		bool debug;
		EngineInitOptions opts;
		std::string error;
		sf::Event ev;
		uzh::InstanceLock engineLock;

		bool initGraphics();
	private:
		// Engine objects
		uzh::Renderer renderer;
		sf::RenderWindow window;
		uzh::Camera cam;
		uzh::SceneManager sceneManager;
		uzh::Game game;
	private:
		/*TESTING*/
		uzh::Shader shader;
		uzh::Texture tex;
		sf::Sprite spr;
		sf::Texture tex2;
		sf::Text debugTxt;
		sf::Font debugFnt;
		uzh::Mesh mesh;
		uzh::Model model;
		uzh::mat4 trans;
		uzh::Skybox skybox;
		uzh::Model ak;
		uzh::Texture aktex;
		float rot;
	private:
		// FPS related stuff
		sf::Clock engineClock;
		sf::Clock frameClock;
		float deltaTime;
		int fpsTime;
		sf::Time frameTime;
		
	};

	//Second and the last global - The Engine Instance
	extern uzh::Engine* UzhEngine;
}