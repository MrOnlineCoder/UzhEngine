/*
	UzhEngine Project

	MIT License

	Copyright (c) 2018 Nikita Kogut (MrOnlineCoder)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	==============================================================================
	File: Entity.cpp
	Description: Basic Scene node
	==============================================================================
*/

#pragma once

#include "Entity.h"

uzh::Entity::Entity(EntityType eid, Entity* parent) {
	this->type = eid;
	this->parent = parent;
	
	transform.pos = uzh::Math::zero3();
	transform.rotation = uzh::Math::zero3();
	transform.scale = uzh::Math::zero3();
}

uzh::EntityType uzh::Entity::getType() {
	return type;
}

uzh::Entity* uzh::Entity::getParent() {
	return parent;
}

void uzh::Entity::update() {

}

void uzh::Entity::processInput(sf::Event ev) {

}
