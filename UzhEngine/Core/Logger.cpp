#include "Logger.h"
namespace uzh {
	uzh::Logger LOG;
}

void uzh::Logger::init() {
	file.open("engine.log");

	file << "--- UzhEngine Session ---";
}

/*uzh::Logger& uzh::Logger::operator<<(int i) {
	file << std::to_string(i);
	return *this;
}*/

uzh::Logger& uzh::Logger::operator<<(std::string s) {
	file << s;
	return *this;
}

uzh::Logger& uzh::Logger::operator<<(float f) {
	file << f;
	return *this;
}

uzh::Logger& uzh::Logger::tag(std::string tag) {
	file << "\n" << std::to_string(clock.getElapsedTime().asSeconds()) << "s. - [" << tag << "] ";
	return *this;
}

void uzh::Logger::close() {
	file.close();
}