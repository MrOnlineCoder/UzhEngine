#include "Player.h"

#include "../Engine/Engine.h"

const float PLAYER_SPEED = 5.0f;

game::Player::Player()
: uzh::Entity(game::EntityTypes::PLAYER) {
	cam = &uzh::UzhEngine->getCamera();
}

void game::Player::update() {
	float delta = uzh::UzhEngine->getDeltaTime();

	transform.pos += accel * delta;

	accel = uzh::Math::zero3();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		accel.x += -glm::cos(glm::radians(transform.rotation.y + 90)) * PLAYER_SPEED;
		accel.z += -glm::sin(glm::radians(transform.rotation.y + 90)) * PLAYER_SPEED;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		accel.x += glm::cos(glm::radians(transform.rotation.y + 90)) * PLAYER_SPEED;
		accel.z += glm::sin(glm::radians(transform.rotation.y + 90)) * PLAYER_SPEED;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		accel.x += glm::cos(glm::radians(transform.rotation.y)) * PLAYER_SPEED;
		accel.z += glm::sin(glm::radians(transform.rotation.y)) * PLAYER_SPEED;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		accel.x += -glm::cos(glm::radians(transform.rotation.y)) * PLAYER_SPEED;
		accel.z += -glm::sin(glm::radians(transform.rotation.y)) * PLAYER_SPEED;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		transform.pos.y += PLAYER_SPEED * delta;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
		transform.pos.y -= PLAYER_SPEED * delta;
	}
}

void game::Player::processInput(sf::Event ev) {
	if (ev.type == sf::Event::MouseMoved) {
		float delta = uzh::UzhEngine->getDeltaTime();

		sf::Vector2i mpos = sf::Mouse::getPosition(uzh::UzhEngine->getWindow());
		float ha = 2.2f * delta * (uzh::UzhEngine->getOptions().winWidth / 2 - mpos.x);
		float va = 2.2f * delta * (uzh::UzhEngine->getOptions().winHeight / 2 - mpos.y);

		//cam->rotateX(-va);
		transform.rotation.x += -va;
		transform.rotation.y += -ha;
		//cam->rotateY(-ha);
	}
}