// UzhEngine Primary Vertex Shader
#version 330 core

//All inputs have "in" Prefix
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;

//All variables, which should be sent to fragment shader, have "f" prefix
out vec2 fTexCoord;
out vec3 fPosition;

//DEFAULT VARIABLES NAMES. DO NOT TOUCH!

//Matrices
uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;
//-------------------------------------

void main() {
  gl_Position = projectionMat * viewMat * modelMat * vec4(inPosition, 1.0);

  fTexCoord = inTexCoord;
  fPosition = vec3(modelMat * vec4(inPosition, 1.0));
}
