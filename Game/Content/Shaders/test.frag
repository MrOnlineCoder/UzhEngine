// UzhEngine Primary Fragment Shader
#version 330 core

//DEFAULT VARIABLES NAMES. DO NOT TOUCH!
out vec4 outFragColor;

uniform sampler2D fTexture;

//-------------------------------------

in vec2 fTexCoord;
in vec3 fPosition;


void main() {
  outFragColor =  texture(fTexture, fTexCoord);
}
