// UzhEngine Skybox Fragment Shader
#version 330 core

out vec4 outFragColor;

uniform sampler2D fTexture;


in vec2 fTexCoord;


void main() {
  outFragColor =  texture(fTexture, fTexCoord);
}
