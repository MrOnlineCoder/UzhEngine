// UzhEngine Skybox Vertex Shader
#version 330 core

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;

out vec2 fTexCoord;

//Matrices
uniform mat4 viewMat;
uniform mat4 projectionMat;
//-------------------------------------

void main() {
  gl_Position = projectionMat * viewMat * vec4(inPosition, 1.0);

  fTexCoord = inTexCoord;
}
