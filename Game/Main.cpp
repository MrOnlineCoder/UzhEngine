#include <UzhEngine.h>
#include <Windows.h>

int main(int argc, char* argv[]) {
	//Create interface from DLL
	IEngineInterface* uzhInterface = InitializeUzhEngine();

	//Init engine and interface
	if (!uzhInterface->init(argc, argv)) {
		return 1;
	}

	//Run !
	int error = uzhInterface->run();

	//Cleanup
	delete uzhInterface;

	return error;
}